import socket
import math
import time

#UDP communication configuration
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
address = ("128.189.223.100", 5002) #User IP address (Target)
sock.bind (address)

coords = []
avg = []
avgtime = 0;

def distance (lat1, lon1, lat2, lon2):
    radius = 6371.0 #Approximate radius of the Earth. DIfference with expected result might be because of this.

    #Convert degrees into radians
    latitude1 = math.radians(lat1)
    latitude2 = math.radians(lat2)
    longitude1 = math.radians(lon1)
    longitude2 = math.radians(lon2)

    #DIfference in latitude and longitude calculation
    dlat = lat2 - lat1
    dlon = lon2 - lon1
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos((lat1)) * math.cos((lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c * 10

    return d

while True:
    start = time.time() #Start counting the time elapsed during each GPS point recieved in order to calculate speed.

    data , addr = sock.recvfrom(1024) #UDP receive protocol

    data2 = data.split(bytes('\t','UTF-8')) #Temporary list with the latitude and longitude recieved. Updated every iteration

    #Parsing the recieved information to float
    latitude = str(data2[0],'UTF-8')
    latitude = latitude.lstrip()
    latitude = float(latitude)

    longitude = str(data2[1],'UTF-8')
    longitude = longitude.rstrip()
    longitude = float(longitude)

    #Inclusion of the latitude and longitude to the list storing the coordinates.
    coords.append (latitude)
    coords.append (longitude)

    #We run a for loop when we have enough setpoints to calculate the distance (4 GPS coordinates needed)
    if (len(coords)==4):

        end = time.time() #We take the current time in order to substract it from the initial one below.
        insttime = end-start
        avgtime = avgtime + insttime #Summation of the current time with the previous one in order to have the total time of the run.
        gps_distance = distance (coords[0], coords[1], coords[2], coords[3]) #We call the distance function
        avg.append(gps_distance) #Inclusion of the recently calculated distance to the list including all the distances in order to calculate the total distance later in the code.
        print ("Current speed: " + str(gps_distance/insttime) + " m/s")
        print ("Averge speed: " + str((sum(avg)/avgtime)) + " m/s")
        #Deletion of the used GPS coordinates (lat1 and lat2)
        del coords[0]
        del coords[0]
