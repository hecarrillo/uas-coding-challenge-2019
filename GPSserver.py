import socket
import datetime, time
import sys
import time

MCAST_GRP = "128.189.223.100" #Target IP address
MCAST_PORT = 5002 #Target port
MULTICAST_TTL = 4

#UDP communication configuration

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)

#GPS waypoins reading

f = open("missionwaypoints.txt","r")

#GPS position updated every second

for x in f:
    print(x)
    sock.sendto(bytes(x,'UTF-8'), (MCAST_GRP, MCAST_PORT))
    time.sleep(1)
